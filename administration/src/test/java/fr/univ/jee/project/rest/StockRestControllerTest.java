package fr.univ.jee.project.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ.jee.project.ApplicationTestBase;
import fr.univ.jee.project.ProjectApplication;
import fr.univ.jee.project.domain.Stock;
import fr.univ.jee.project.domain.resource.Resource;
import fr.univ.jee.project.domain.resource.VideoGame;
import fr.univ.jee.project.repository.StockRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;


public class StockRestControllerTest extends ApplicationTestBase {


    @Autowired
    private StockRepository stockRepository;

    @Test
    public void getAllAd() throws Exception {

        this.mockMvc.perform(get("/stock"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize((int)stockRepository.count())));
    }

    @Test
    public void getById() throws Exception {
        Stock stock = stockRepository.findAll().iterator().next();
        this.mockMvc.perform(get("/stock/" + stock.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(Math.toIntExact(stock.getId()))));
    }


    @Test
    public void getByIdNotFound() throws Exception {
        this.mockMvc.perform(get("/stock/" + 100000))
                .andExpect(status().isNotFound());
    }


    @Test
    public void add() throws Exception {

        Resource resource = new VideoGame();
        resource.setName("Overwatch");

        Stock stock = new Stock();
        stock.setQuantity(10);
        stock.setResource(resource);

        String videoGame = json(resource);

        this.mockMvc.perform(post("/stock")
                .contentType(contentType)
                .content(videoGame))
                .andExpect(status().isCreated());

    }

    @Test
    public void updateResource() throws Exception {
        Stock stock = stockRepository.findAll().iterator().next();
        int q = 1000000;
        stock.setQuantity(q);
        String stockJson = json(stock);

         this.mockMvc.perform(put("/stock/" + stock.getId())
                .contentType(contentType)
                .content(stockJson))
                .andExpect(status().isOk());
         assertThat(stockRepository.findById(stock.getId()).get().getQuantity(), is(q));
    }


    @Test
    public void updateResourceNotFound() throws Exception {
        Stock stock = stockRepository.findAll().iterator().next();
        int q = 1000000;
        stock.setQuantity(q);
        String stockJson = json(stock);

        this.mockMvc.perform(put("/stock/" + 10000000)
                .contentType(contentType)
                .content(stockJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteStock() throws Exception {
        Stock s = new Stock();
        s = stockRepository.save(s);

        this.mockMvc.perform(delete("/stock/" + s.getId()))
                .andExpect(status().isOk());

        assertThat(stockRepository.findById(s.getId()).isPresent(), is(false));
    }


    @Test
    public void importStock() throws Exception {
        VideoGame resourceNew = new VideoGame();
        resourceNew.setName("newGame");
        resourceNew.setId(100000L);

        Stock stockNew = new Stock();
        stockNew.setResource(resourceNew);
        stockNew.setQuantity(10);
        Long stockNb = stockRepository.count();
        Stock oldStock = stockRepository.findAll().iterator().next();
        Stock[] stocks = {stockNew, oldStock};


        this.mockMvc.perform(MockMvcRequestBuilders.fileUpload("/stock/import")
                .file("file", (new ObjectMapper()).writeValueAsBytes(stocks))
                .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isOk());

        assertThat(stockRepository.count(), is(stockNb+1));
        assertThat(stockRepository.findById(oldStock.getId()).get().getQuantity(), is(oldStock.getQuantity()*2));

    }


}