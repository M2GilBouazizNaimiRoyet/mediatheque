package fr.univ.jee.project.rest;

import fr.univ.jee.project.ApplicationTestBase;
import fr.univ.jee.project.domain.Borrowing;
import fr.univ.jee.project.domain.Stock;
import fr.univ.jee.project.domain.User;
import fr.univ.jee.project.domain.resource.Resource;
import fr.univ.jee.project.domain.resource.VideoGame;
import fr.univ.jee.project.repository.ResourceRepository;
import fr.univ.jee.project.repository.UserRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BorrowingRestControllerTest extends ApplicationTestBase {

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private UserRepository userRepository;


    @Test
    public void getAllAd() throws Exception {
    }

    @Test
    public void getById() throws Exception {
    }

    @Test
    public void add() throws Exception {

        Borrowing borrowing = new Borrowing();
        borrowing.setResource(resourceRepository.findAll().iterator().next());
        borrowing.setDuration(10);
        borrowing.setDate("10/10/2018");
        borrowing.setBorrower(userRepository.findAll().iterator().next());

        String borrowingJson = json(borrowing);

        this.mockMvc.perform(post("/borrowing")
                .contentType(contentType)
                .content(borrowingJson))
                .andExpect(status().isCreated());


    }

    @Test
    public void delete() throws Exception {
    }

}