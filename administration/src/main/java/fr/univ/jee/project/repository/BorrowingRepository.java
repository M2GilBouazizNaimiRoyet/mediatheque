package fr.univ.jee.project.repository;

import fr.univ.jee.project.domain.Ad;
import fr.univ.jee.project.domain.Borrowing;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BorrowingRepository extends CrudRepository<Borrowing, Long> {

    List<Borrowing> findAll();

    Optional<Borrowing> findById(Long id);
}
