package fr.univ.jee.project.soap;

import fr.univ.jee.project.repository.BorrowingRepository;
import fr.univ.jee.project.repository.UserRepository;
import fr.univ.jee.project.security.AuthoritiesConstants;
import fr.univ.jee.project.soap.dto.GetBorrowingsRequest;
import fr.univ.jee.project.soap.dto.GetBorrowingsResponse;
import fr.univ.jee.project.soap.dto.GetUsersRequest;
import fr.univ.jee.project.soap.dto.GetUsersResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class MediathequeEndpoint {
    private static final String NAMESPACE_URI = "http://jee-project-univ-rouen.fr";

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BorrowingRepository borrowingRepository;


	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUsersRequest")
	@ResponsePayload
	public GetUsersResponse getUsers(@RequestPayload GetUsersRequest request) {
		GetUsersResponse response = new GetUsersResponse();
		response.setUsers(userRepository.findByAuthorityName(AuthoritiesConstants.USER));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBorrowingsRequest")
	@ResponsePayload
	public GetBorrowingsResponse getBorrowings(@RequestPayload GetBorrowingsRequest request) {
		GetBorrowingsResponse response = new GetBorrowingsResponse();
		response.setBorrowings(borrowingRepository.findAll());
		return response;
	}
}