package fr.univ.jee.project.rest;


import fr.univ.jee.project.domain.Ad;
import fr.univ.jee.project.exception.NotFoundException;
import fr.univ.jee.project.repository.AdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@RestController
@RequestMapping("/ad")
public class AdRestController {

    @Autowired
    private AdRepository adRepository;

    @GetMapping
    public Iterable<Ad> getAllAd() {
        return adRepository.findAll();
    }

    @GetMapping("/{id}")
    public Ad getById(@PathVariable Long id) {
        return adRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody Ad ad) {
        adRepository.save(ad);
    }


    @PutMapping("/{id}")
    @Transactional
    public Ad updateResource(@PathVariable Long id, @RequestBody Ad ad) {
        return adRepository.findById(id)
                .map((Ad a) -> {
                    a.setContent(ad.getContent());
                    return a;
                }).orElseThrow(NotFoundException::new);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        adRepository.delete(id);
    }

}
