package fr.univ.jee.project.rest;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ.jee.project.domain.Stock;
import fr.univ.jee.project.domain.resource.Resource;
import fr.univ.jee.project.exception.NotFoundException;
import fr.univ.jee.project.repository.ResourceRepository;
import fr.univ.jee.project.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@RestController
@RequestMapping("/stock")
public class StockRestController {

    @Autowired
    private ObjectMapper om;

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private ResourceRepository resourceRepository;

    @GetMapping
    public Iterable<Stock> getAllAd() {
        return stockRepository.findAll();
    }

    @GetMapping("/{id}")
    public Stock getById(@PathVariable Long id) {
        return stockRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody Stock stock) {
        stockRepository.save(stock);
    }

    @PutMapping("/{id}")
    @Transactional
    public Stock updateResource(@PathVariable Long id,
                                   @RequestBody Stock stock) {
        return stockRepository.findById(id)
                .map((Stock r) -> {
                    r.setQuantity(stock.getQuantity());

                    //use existing resource if already exist
                    Resource resource =resourceRepository.findById(stock.getResource().getId())
                            .orElse(stock.getResource());

                    r.setResource(resource);

                    return r;
                }).orElseThrow(NotFoundException::new);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        stockRepository.delete(id);
    }

    @PostMapping("/import")
    @Transactional
    public void importResource(@RequestParam("file") MultipartFile file) throws IOException {
        Stock[] stocks = om.readValue(file.getBytes(), Stock[].class);
        Stream.of(stocks).forEach(s -> {
           boolean isPresent = stockRepository.findById(s.getId()).map(stock -> {
               stock.setQuantity(stock.getQuantity() + s.getQuantity());
               return stock;
           }).isPresent();

           if (!isPresent) {
               Resource resource = resourceRepository.findById(s.getResource().getId()).orElse(s.getResource());
               resource = resourceRepository.save(resource);
               s.setResource(resource);

               stockRepository.save(s);
           }
        });
    }

    @GetMapping("/export/json")
    public HttpEntity<byte[]> export() throws JsonProcessingException {
        byte[] exportFile = om.writeValueAsBytes(stockRepository.findAll());

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=export.json" );
        header.setContentLength(exportFile.length);

        return new HttpEntity<byte[]>(exportFile, header);
    }
}
