package fr.univ.jee.project.repository;

import fr.univ.jee.project.domain.Ad;
import fr.univ.jee.project.domain.Authority;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorityRepository extends CrudRepository<Authority, String> {

    Optional<Authority> findByName(String name);

}
