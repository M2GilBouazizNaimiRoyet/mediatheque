package fr.univ.jee.project.security;

import fr.univ.jee.project.domain.User;
import fr.univ.jee.project.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class DomainUserDetailsService implements UserDetailsService {

    private Logger log = LoggerFactory.getLogger(DomainUserDetailsService.class);
    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(final String login) {
        Optional<User> userLoaded = userRepository.findByEmail(login);
        log.debug("username found {}, password : {}", userLoaded.get().getEmail(), userLoaded.get().getPassword());
        return userLoaded.map((User user) -> {

           List<GrantedAuthority> grantedAuthorities = Arrays.asList(new SimpleGrantedAuthority(user.getAuthority().getName()));

            return new org.springframework.security.core.userdetails.User(user.getEmail(),
                    user.getPassword(),
                    grantedAuthorities);
        }).orElseThrow(() -> new UsernameNotFoundException("User " + login + "not found" ));
    }
}