package fr.univ.jee.project.rest;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ.jee.project.domain.Borrowing;
import fr.univ.jee.project.exception.NotFoundException;
import fr.univ.jee.project.repository.BorrowingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/borrowing")
public class BorrowingRestController {

    @Autowired
    private BorrowingRepository borrowingRepository;

    @Autowired
    private ObjectMapper om;

    @GetMapping("/export/json")
    public HttpEntity<byte[]> export() throws JsonProcessingException {
        byte[] exportFile = om.writeValueAsBytes(borrowingRepository.findAll());

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=export.json" );
        header.setContentLength(exportFile.length);

        return new HttpEntity<byte[]>(exportFile, header);
    }

    @GetMapping
    public List<Borrowing> getAllAd(@RequestParam(required = false) Long userId) {
        List<Borrowing> borrowings =  borrowingRepository.findAll();

        if (userId != null) {
            borrowings = borrowings.stream()
                    .filter(borrowing -> borrowing.getBorrower().getId().equals(userId))
                    .collect(Collectors.toList());
        }

        return borrowings;
    }

    @GetMapping("/{id}")
    public Borrowing getById(@PathVariable Long id) {
        return borrowingRepository.findById(id)
                .orElseThrow(NotFoundException::new);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody Borrowing borrowing) {
        borrowingRepository.save(borrowing);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        borrowingRepository.delete(id);
    }
}
