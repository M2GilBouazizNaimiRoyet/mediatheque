package fr.univ.jee.project.repository;

import fr.univ.jee.project.domain.Borrowing;
import fr.univ.jee.project.domain.resource.Resource;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ResourceRepository extends CrudRepository<Resource, Long> {
    Optional<Resource> findById(Long id);
}
