package fr.univ.jee.project;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ.jee.project.domain.*;
import fr.univ.jee.project.domain.resource.CD;
import fr.univ.jee.project.domain.resource.Resource;
import fr.univ.jee.project.domain.resource.VideoGame;
import fr.univ.jee.project.repository.*;
import fr.univ.jee.project.security.AuthoritiesConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AdRepository adRepository;

    @Autowired
    private BorrowingRepository borrowingRepository;

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private ResourceRepository resourceRepository;
    private User user;
    private Resource resource;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        borrowingRepository.deleteAll();
        userRepository.deleteAll();
        stockRepository.deleteAll();
        resourceRepository.deleteAll();
        adRepository.deleteAll();

        saveUsers();
        saveResource();
        saveBorrowing();
        saveAd();
    }

    private void saveUsers() {

        user = new User();
        user.setEmail("email@email.fr");
        user.setPassword(passwordEncoder.encode("password"));

        Authority userAuthority = new Authority();
        userAuthority.setName(AuthoritiesConstants.USER);
        authorityRepository.save(userAuthority);
        user.setAuthority(userAuthority);

        User admin = new User();
        admin.setEmail("admin@email.fr");
        Authority adminAuthority = new Authority();
        adminAuthority.setName(AuthoritiesConstants.ADMIN);
        authorityRepository.save(adminAuthority);
        admin.setAuthority(adminAuthority);
        admin.setPassword(passwordEncoder.encode("admin"));


        userRepository.save(user);
        userRepository.save(admin);
    }

    private void saveResource() {

        resource = new VideoGame();
        resource.setName("Overwatch");

        Stock stock = new Stock();
        stock.setQuantity(10);
        stock.setResource(resource);
        stockRepository.save(stock);

        CD resource1 = new CD();
        resource1.setName("Une musique");
        resource1.setAlbumName("album1");


        Stock stock1 = new Stock();
        stock1.setQuantity(2);
        stock1.setResource(resource1);

        stockRepository.save(stock1);
    }

    private void saveBorrowing() {


        Borrowing borrowing = new Borrowing();
        borrowing.setBorrower(user);
        borrowing.setDate(LocalDateTime.now().minusDays(5).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        borrowing.setResource(resource);
        borrowing.setDuration(7);

        borrowingRepository.save(borrowing);
    }

    private void saveAd() {


        Ad ad = new Ad();
        ad.setContent("Vous allez bien ?");
        ad.setPublishDate(LocalDateTime.now().minusDays(5).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

        adRepository.save(ad);
    }


}

