package fr.univ.jee.project.rest.dto;

public class PasswordChangeDto {

    private String password;

    private String oldPassword;
    public PasswordChangeDto() {}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
