package fr.univ.jee.project.repository;

import fr.univ.jee.project.domain.Ad;
import fr.univ.jee.project.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdRepository extends CrudRepository<Ad, Long> {

    Optional<Ad> findById(Long id);
}
