package fr.univ.jee.project.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ.jee.project.domain.User;
import fr.univ.jee.project.exception.NotFoundException;
import fr.univ.jee.project.repository.UserRepository;
import fr.univ.jee.project.rest.dto.PasswordChangeDto;
import fr.univ.jee.project.rest.dto.UserDto;
import fr.univ.jee.project.security.SecurityUtils;
import fr.univ.jee.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
public class UserController {


    @Autowired
    private ObjectMapper om;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    

    @GetMapping("/user")
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/user/{id}")
    public User getById(@PathVariable Long id) {
        return userRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    @DeleteMapping("/user/{id}")
    public void deleteUser(@PathVariable Long id) {
        userRepository.delete(id);
    }

    @PostMapping("/user")
    @ResponseStatus(HttpStatus.CREATED)
    public User addUser(@RequestBody @Valid UserDto user) {
        return userService.register(user.toUser());
    }


    @GetMapping("/account/{id}")
    public User getAccount(@PathVariable Long id) {
     //  return userRepository.findById(id)
       //        .orElseThrow(NotFoundException::new);
         return userRepository.findByEmail(SecurityUtils.getCurrentUserUsername())
                .orElseThrow(() -> new UsernameNotFoundException("username not found"));
    }

    @GetMapping("/account")
    public User getAccount() {
        //  return userRepository.findById(id)
        //        .orElseThrow(NotFoundException::new);
        return userRepository.findByEmail(SecurityUtils.getCurrentUserUsername())
                .orElseThrow(() -> new UsernameNotFoundException("username not found"));
    }


    @PutMapping("/account")
    public void updateAccount(@RequestBody @Valid PasswordChangeDto passwordChangeDto) {
        userService.changePassword(SecurityUtils.getCurrentUserUsername(),
                passwordChangeDto.getPassword(),
                passwordChangeDto.getOldPassword());
    }

    @PutMapping("/account/{id}")
    public void updateAccount(@PathVariable Long id,
                              @RequestBody @Valid PasswordChangeDto passwordChangeDto) {
        userService.changePassword(SecurityUtils.getCurrentUserUsername(),
                passwordChangeDto.getPassword(),
                passwordChangeDto.getOldPassword());
    }


    @GetMapping("/user/export/json")
    public HttpEntity<byte[]> export() throws JsonProcessingException {
        byte[] exportFile = om.writeValueAsBytes(userRepository.findAll());

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=export.json" );
        header.setContentLength(exportFile.length);

        return new HttpEntity<byte[]>(exportFile, header);
    }

}
