package fr.univ.jee.project.rest.dto;

import fr.univ.jee.project.domain.Authority;
import fr.univ.jee.project.domain.User;


public class UserDto {

    private String email;

    private String password;

    private Authority authority;

    public UserDto() {}


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Authority getAuthority() {
        return authority;
    }

    public void setAuthority(Authority authority) {
        this.authority = authority;
    }

    public User toUser() {
        User user = new User();
        user.setEmail(getEmail());
        user.setPassword(getPassword());
        user.setAuthority(getAuthority());
        return user;
    }
}
