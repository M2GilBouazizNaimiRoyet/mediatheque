package fr.univ.jee.project.soap.dto;

import fr.univ.jee.project.domain.Borrowing;
import fr.univ.jee.project.domain.User;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class GetBorrowingsResponse {
    private List<Borrowing> borrowings;

    public GetBorrowingsResponse() {}

    @XmlElementWrapper(name="borrowings")
    @XmlElement(name="borrowing")
    public List<Borrowing> getBorrowings() {
        return borrowings;
    }

    public void setBorrowings(List<Borrowing> borrowings) {
        this.borrowings = borrowings;
    }
}
