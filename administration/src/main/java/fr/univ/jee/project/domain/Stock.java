package fr.univ.jee.project.domain;

import fr.univ.jee.project.domain.resource.Resource;

import javax.persistence.*;

@Entity
public class Stock {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @OneToOne(cascade = {CascadeType.ALL})
    private Resource resource;


    @Column
    private int quantity;

    public Stock() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }



    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Stock stock = (Stock) o;

        if (quantity != stock.quantity) return false;
        if (!id.equals(stock.id)) return false;
        return resource.equals(stock.resource);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + resource.hashCode();
        result = 31 * result + quantity;
        return result;
    }
}


