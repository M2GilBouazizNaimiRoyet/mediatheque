package fr.univ.jee.project.soap.dto;

import fr.univ.jee.project.domain.User;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class GetUsersResponse {
    private List<User> users;

    public GetUsersResponse() {}

    @XmlElementWrapper(name="users")
    @XmlElement(name="user")
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
