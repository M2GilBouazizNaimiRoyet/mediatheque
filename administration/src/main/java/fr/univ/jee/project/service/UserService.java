package fr.univ.jee.project.service;

import fr.univ.jee.project.domain.Authority;
import fr.univ.jee.project.domain.User;
import fr.univ.jee.project.exception.NotFoundException;
import fr.univ.jee.project.exception.UsernameAlreadyUsedException;
import fr.univ.jee.project.repository.AuthorityRepository;
import fr.univ.jee.project.repository.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
@Service
@Transactional
public class UserService {

    Logger log = Logger.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserService() {}

    public User register(User user) {
        userRepository.findByEmail(user.getEmail()).ifPresent(u -> {
            throw new UsernameAlreadyUsedException();
        });

        // save a new Authority or get existing one with equal name
        authorityRepository.findByName(user.getAuthority().getName())
                .map(auth -> {
                    user.setAuthority(auth);
                    return user;
                })
                .orElseGet(() -> {
                        authorityRepository.save(user.getAuthority());
                        return user;
                });

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return user;
    }

    public void changePassword(String email, String password, String oldPassword) {
        User user = userRepository.findByEmail(email)
                .orElseThrow(NotFoundException::new);

        if (!passwordEncoder.matches(oldPassword, user.getPassword())) throw new BadCredentialsException("bad credentials");

        user.setPassword(passwordEncoder.encode(password));
    }
}
