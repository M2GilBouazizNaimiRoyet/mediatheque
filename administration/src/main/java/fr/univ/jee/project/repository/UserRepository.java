package fr.univ.jee.project.repository;

import fr.univ.jee.project.domain.User;
import fr.univ.jee.project.security.AuthoritiesConstants;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository  extends CrudRepository<User, Long> {

    Optional<User> findByEmail(String login);

    Optional<User> findById(Long id);

    List<User> findByAuthorityName(String admin);
}
