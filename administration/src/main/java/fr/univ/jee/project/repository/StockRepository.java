package fr.univ.jee.project.repository;

import fr.univ.jee.project.domain.Stock;
import fr.univ.jee.project.domain.resource.Resource;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StockRepository extends CrudRepository<Stock, Long> {

    Optional<Stock> findById(Long id);

    Optional<Stock> findByResourceId(Long id);
}
