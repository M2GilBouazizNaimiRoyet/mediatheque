import React, { Component } from "react";
import axios from "axios";
import Loading from './Loading';
import FileSaver from 'file-saver';

class LoaningRow extends Component {

  render() {
    const loaning = this.props.loaning;
    var classname;
    if (loaning.duration >= 7) {
      classname = "warning";
    }
    return (
      <tr key={loaning.id} className={classname}>
        <td >
        {loaning.resource.name}
        </td>
        <td>
          {loaning.resource.type}
        </td>
        <td>
          {loaning.date}
        </td>
        <td>
          {loaning.duration}
        </td>

        <td>
          {loaning.borrower.email}
        </td>
      </tr>
    );
  }
}
class ExportLoaningsButton extends Component {
    constructor(props) {
        super(props);
        this.handleExport = this.handleExport.bind(this);
    }


    handleExport(event) {
        var apiBaseUrl = "http://localhost:8080/project-1.0/";
        axios.get(apiBaseUrl+'borrowing/export/json', { withCredentials: true }).then((response) => {
            var blob = new Blob([JSON.stringify(response.data)], {type: "application/json;charset=utf-8"});
            FileSaver.saveAs(blob, 'borrowings.json');
        });
    }

    render() {
        return (
            <button onClick={this.handleExport}>
                exporter les utilisateurs
            </button>
        );
    }
}

class Loanings extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loanings: [],
      isCharged : false
    };
  }

  componentDidMount(){
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    var self = this;
     axios.get(apiBaseUrl+'borrowing', { withCredentials: true })
     .then(function (response) {
       if(response.status === 200){
         self.setState({
           loanings : response.data,
           isCharged : true
         })
       }
     });
  }

    componentDidUpdate(){
        var apiBaseUrl = "http://localhost:8080/project-1.0/";
        var self = this;
        axios.get(apiBaseUrl+'borrowing', { withCredentials: true })
            .then(function (response) {
                if(response.status === 200 && JSON.stringify(self.state.loanings)!==JSON.stringify(response.data)){
                    self.setState({
                        loanings : response.data,
                        isCharged : true
                    })
                }
            });
    }

  render() {
    if (!this.state.isCharged) {
      return (<Loading/>);
    }
    const rows = [];
    this.state.loanings.forEach((loaning) => {
      rows.push(<LoaningRow loaning={loaning} key={loaning.id} />)
    });
    return (
        <div>
            <table>
              <thead>
                <tr>
                  <th>Nom</th>
                  <th>Type</th>
                  <th>Début</th>
                  <th>Durée</th>
                  <th>Emprunteur</th>
                </tr>
              </thead>
              <tbody>{rows}</tbody>
            </table>
            <ExportLoaningsButton/>
        </div>
    );
  }
}

export default Loanings;
