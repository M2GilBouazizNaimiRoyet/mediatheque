import React, { Component } from "react";
import {Redirect} from "react-router-dom";
import Loading from './Loading';
import axios from "axios";

class AddResource extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name : '',
      type : 'DVD',
      quantity : 0,
      redirect: false,
      isChanging:false
    };

    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeType = this.handleChangeType.bind(this);
    this.handleChangeQuantity = this.handleChangeQuantity.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeName(event) {
    this.setState({name: event.target.value});
  }

  handleChangeType(event) {
    this.setState({type: event.target.value});
  }

  handleChangeQuantity(event) {
    this.setState({quantity: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    var self = this;
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    var payload={
      "quantity":this.state.quantity,
       "resource": {
         "type":this.state.type,
         "name":this.state.name
       }
     }
     self.setState({ isChanging: true });
    axios.post(apiBaseUrl+'stock', payload, { withCredentials: true })
    .then(function (response) {
      if(response.status === 201) {
        self.setState({ redirect: true });
      }
    });
  }

  render() {
    const { redirect } = this.state;
    if (this.state.isChanging) {
      if (redirect) {
            return <Redirect to={'/Administrateur/stocks'}/>;
      }
      return <Loading/>;
    }
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Nom :
            <input type="text"
              value={this.state.name} onChange={this.handleChangeName}/>
          </label>
          <br/>
          <label>
            <select value={this.state.type}
              onChange={this.handleChangeType}>
              <option value="DVD">DVD</option>
              <option value="CD">CD</option>
              <option value="VideoGame">jeu vidéo</option>
              <option value="BoardGame">jeu de société</option>
              <option value="Book">livre</option>
            </select>
          </label>
          <br/>
          <label>
            Quantité :
            <input type="number"
              value={this.state.quantity} onChange={this.handleChangeQuantity}/>
          </label>
          <br/>
          <input type="submit" value="Ajouter" />
        </form>
      </div>
    );
  }
}

export default AddResource;
