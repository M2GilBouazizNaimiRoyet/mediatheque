import React, { Component } from "react";
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import {Redirect} from "react-router-dom";
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import axios from "axios";
import Loading from './Loading';


class ModifyAd extends Component {

  constructor(props) {
    super(props);
    this.state = {
      editorState : undefined,
      ad : undefined,
      redirect: false,
      isChanging:false,
      isCharged:false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    var self = this;
     axios.get(apiBaseUrl+'ad/'+this.props.id, { withCredentials: true })
     .then(function (response) {
       if(response.status === 200){
         const contentBlock = htmlToDraft(response.data.content);
         const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
         const editorState = EditorState.createWithContent(contentState);
         self.setState({
           editorState,
           ad : response.data,
           isCharged:true

         });
       }
     });
  }

  onEditorStateChange: Function = (editorState) => {
    this.setState({
      editorState,
    });
  };

  handleSubmit(event) {
    event.preventDefault();
    var self = this;
    const { editorState } = this.state;
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    var payload={
       "content":draftToHtml(convertToRaw(editorState.getCurrentContent()))
     }
     self.setState({ isChanging: true });
    axios.put(apiBaseUrl+'ad/'+this.props.id, payload, { withCredentials: true })
    .then(function (response) {
      if(response.status === 200) {
        self.setState({ redirect: true });
      }
    });
  }

  render() {
    const { redirect } = this.state;
    if (!this.state.isCharged) {
      return (<Loading/>);
    }
    if (this.state.isChanging) {
      if (redirect) {
            return <Redirect to={'/Administrateur/annonces'}/>;
      }
      return <Loading/>;
    }
    return (
      <form onSubmit={this.handleSubmit}>
        <Editor
          editorState={this.state.editorState}
          toolbarClassName="toolbarClassName"
          wrapperClassName="demo-wrapper"
          editorClassName="demo-editor"
          onEditorStateChange={this.onEditorStateChange}
          localization={{
            locale: 'fr',
          }}
        />
        <input type="submit" value="Modifier" />
      </form>
    );
  }
}

class RemoveAdButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect : false,
      isDeleting : false
    }
    this.handleRemove = this.handleRemove.bind(this);
  }

  handleRemove(event) {
    var self = this;
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    self.setState({ isDeleting: true });
    axios.delete(apiBaseUrl+'ad/'+this.props.id, { withCredentials: true })
    .then(function (response) {
      if(response.status === 200) {
        self.setState({ redirect: true });
      }
    });
  }

  render() {
    const { redirect } = this.state;
    if (this.state.isDeleting) {
      if (redirect) {
            return <Redirect to={'/Administrateur/annonces'}/>;
      }
      return <Loading/>;
    }
    return (
        <button onClick={this.handleRemove}>
          supprimer cette annonce
        </button>
    );
  }
}

class HandleAd extends Component {
  render() {
    const path = this.props.location;
    const idUser = path.substr(path.lastIndexOf(('/')) + 1);
    return (
      <div>
        <RemoveAdButton id={idUser}/>
        <ModifyAd id={idUser}/>
      </div>
    );
  }
}

export default HandleAd;
