import React, { Component } from "react";
import {Link} from "react-router-dom";
import axios from "axios";
import styles from './index.css';
import Loading from './Loading';
import FileSaver from 'file-saver';


class ResourceRow extends Component {

  render() {
    const resource = this.props.resource;
    return (
      <tr key={resource.id}>
        <td>
          <Link className={styles.contenttd} to={`/Administrateur/stocks/${resource.id}`}>
            {resource.resource.name}
          </Link>
        </td>
        <td>
          <Link className={styles.contenttd} to={`/Administrateur/stocks/${resource.id}`}>
            {resource.resource.type}
          </Link>
        </td>
        <td>
          <Link className={styles.contenttd} to={`/Administrateur/stocks/${resource.id}`}>
            {resource.quantity}
          </Link>
        </td>
      </tr>
    );
  }
}

class StocksTab extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stocks: [],
      isCharged : false
    };
  }

  componentDidMount(){
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    var self = this;
     axios.get(apiBaseUrl+'stock', { withCredentials: true })
     .then(function (response) {
       if(response.status === 200){
         self.setState({
           stocks : response.data,
           isCharged : true
         })
       }
     });
  }

    componentDidUpdate(){
        var apiBaseUrl = "http://localhost:8080/project-1.0/";
        var self = this;
        axios.get(apiBaseUrl+'stock', { withCredentials: true })
            .then(function (response) {
                if(response.status === 200 && JSON.stringify(self.state.stocks)!==JSON.stringify(response.data)){
                    self.setState({
                        stocks : response.data,
                        isCharged : true
                    })
                }
            });
    }
  render() {
    if (!this.state.isCharged) {
      return (<Loading/>);
    }
    const rows = [];
    this.state.stocks.forEach((resource) => {
      rows.push(<ResourceRow resource={resource} key={resource.id} />)
    });
    return (
        <table>
          <thead>
            <tr>
              <th>Nom</th>
              <th>Type</th>
              <th>Quantité</th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
    );
  }
}

class AddResourceButton extends Component {
  render() {
    return (
      <Link to={`/Administrateur/stocks/add`}>
        <button>
          ajouter une ressource
        </button>
      </Link>
    );
  }
}

class ExportStocksButton extends Component {
    constructor(props) {
        super(props);
        this.handleExport = this.handleExport.bind(this);
    }


    handleExport(event) {
        var apiBaseUrl = "http://localhost:8080/project-1.0/";
        axios.get(apiBaseUrl+'stock/export/json', { withCredentials: true }).then((response) => {
            var blob = new Blob([JSON.stringify(response.data)], {type: "application/json;charset=utf-8"});
            FileSaver.saveAs(blob, 'stocks.json');
        });
    }

    render() {
        return (
            <button onClick={this.handleExport}>
                exporter les stocks
            </button>
        );
    }
}

class ImportStocksButton extends Component {
    constructor(props) {
        super(props);
        this.state ={
            file:null,
            isSending : false,
            refresh:false
        }
        this.onChange = this.onChange.bind(this)
        this.handleImport = this.handleImport.bind(this);
    }

    onChange(e) {
        this.setState({file:e.target.files[0]})
    }

    handleImport(event) {
        event.preventDefault();
        this.setState({isSending:true});
        let data = new FormData();
        var self = this;
        data.append('file',this.state.file);
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            },
            withCredentials: true
        };
        var apiBaseUrl = "http://localhost:8080/project-1.0/";
        axios.post(apiBaseUrl+'stock/import', data, config)
            .then(function (response) {
                self.setState({
                    isSending:false
                })
                self.props.refresh()
            });

    }
    render() {
        if (this.state.isSending) {
            return (<Loading/>);
        }
        return (
            <form onSubmit={this.handleImport} >
                importer des stocks : <input type="file" onChange={this.onChange} />
                <input type="submit" value="Envoyer le fichier" />
            </form>
        );
    }
}

class Stocks extends Component {
    constructor(props) {
        super(props);
        this.state ={
            refresh : false
        }
        this.refresh = this.refresh.bind(this)

    }

   refresh() {
        this.setState({refresh:true});
   }


  render() {
    return (
      <div>
        <AddResourceButton/>
        <StocksTab/>
        <ExportStocksButton/>
          <ImportStocksButton refresh={this.refresh}/>
      </div>
    )
  }
}


export default Stocks;