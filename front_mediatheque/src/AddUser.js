import React, { Component } from "react";
import {Redirect} from "react-router-dom";
import axios from "axios";
import Loading from './Loading';

class AddUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mail : '',
      role : 'USER',
      password: '',
      redirect: false,
      isChanging:false
    };

    this.handleChangeMail = this.handleChangeMail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleChangeRole = this.handleChangeRole.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeMail(event) {
    this.setState({mail: event.target.value});
  }

  handleChangePassword(event) {
    this.setState({password: event.target.value});
  }

  handleChangeRole(event) {
    this.setState({role: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    var self = this;
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    var payload={
       "email":this.state.mail,
       "password":this.state.password,
       "authority":{"name":this.state.role}
    }
    self.setState({ isChanging: true });
    axios.post(apiBaseUrl+'user', payload, { withCredentials: true })
    .then(response => {
            if (response.status === 201) {
                self.setState({redirect: true});
            }
        },
        error => {
            alert("L'utilisateur existe déjà.");
            self.setState({ isChanging: false });
        });
  }

  render() {
    const { redirect } = this.state;
    if (this.state.isChanging) {
      if (redirect) {
            return <Redirect to={'/Administrateur/users'}/>;
      }
      return <Loading/>;
    }
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Mail :
            <input type="email"
              value={this.state.mail} onChange={this.handleChangeMail}/>
          </label>
          <br/>
          <label>
            Mot de passe :
            <input type="password"
              value={this.state.password} onChange={this.handleChangePassword}/>
          </label>
          <br/>
          <label>
            Rôle :
            <select value={this.state.role}
              onChange={this.handleChangeRole}>
              <option value="USER">Utilisateur</option>
              <option value="ADMIN">Administrateur</option>
            </select>
          </label>
        <br/>
          <input type="submit" value="Ajouter" />
        </form>
      </div>
    );
  }
}

export default AddUser;
