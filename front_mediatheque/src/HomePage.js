import React, { Component } from "react";
import {
  Route,
  NavLink,
  HashRouter,
  Switch,
  Redirect
} from "react-router-dom";
import Home from "./Home";
import Login from "./Login";
import Users from "./Users";
import Stocks from "./Stocks";
import Loanings from "./Loanings";
import Ads from "./Ads";
import AddUser from "./AddUser";
import HandleUser from "./HandleUser";
import AddResource from "./AddResource";
import HandleResource from "./HandleResource";
import AddAd from "./AddAd";
import HandleAd from "./HandleAd";
import Logout from "./Logout";
import Account from "./Account";
import MyLoanings from "./MyLoanings";

class HomePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
          refresh:false
        };
    }

  render() {
    this.isLoggedIn = window.sessionStorage.getItem('isLoggedIn') === 'true';
    this.isAdmin = window.sessionStorage.getItem('isAdmin') === 'true';
    this.mail = window.sessionStorage.getItem('mail');
    const links = [];

    if (this.isAdmin) {
      links.push(<li><NavLink exact to="/Administrateur">Page d'accueil</NavLink></li>);
      links.push(<li><NavLink exact to="/Administrateur/users">Utilisateurs</NavLink></li>);
      links.push(<li><NavLink to="/Administrateur/stocks">Stocks</NavLink></li>);
      links.push(<li><NavLink to="/Administrateur/emprunts">Emprunts</NavLink></li>);
      links.push(<li><NavLink to="/Administrateur/annonces">Annonces</NavLink></li>);
      links.push(<li><NavLink to="/deconnexion">Se Déconnecter</NavLink></li>);
    } else {
      links.push(<li><NavLink exact to="/">Page d'accueil</NavLink></li>);
      if (this.isLoggedIn) {
        links.push(<li><NavLink to="/compte">Mon Compte</NavLink></li>);
        links.push(<li><NavLink to="/emprunts">Mes emprunts</NavLink></li>);
        links.push(<li><NavLink to="/deconnexion">Se Déconnecter</NavLink></li>);
      } else {
        links.push(<li><NavLink to="/connexion">Se Connecter</NavLink></li>);
      }
    }

    return (
      <HashRouter>
        <div>
          <h1>Médiathéque</h1>
          <ul className="header">
            {links}
          </ul>
          <div className="content">
            <Route exact path="/" component={Home}/>
            <Route exact path="/connexion" render={() => (this.isLoggedIn ?
              (<Redirect to="/"/>) : (<Login parent={this}/>))}/>
            <Route exact path="/compte" render={() => (!this.isLoggedIn || this.isAdmin?
              (<Redirect to="/"/>) : (<Account />))}/>
            <Route exact path="/emprunts" render={() => (!this.isLoggedIn || this.isAdmin?
              (<Redirect to="/"/>) : (<MyLoanings />))}/>
            <Route exact path="/deconnexion" render={() => (!this.isLoggedIn ?
              (<Redirect to="/"/>) : (<Logout parent={this}/>))}/>
            <Route exact path="/Administrateur" component={Home}/>
            <Route exact path="/Administrateur/users" render={() => (!this.isAdmin ?
              (<Redirect to="/"/>) : (<Users />))}/>
            <Route exact path="/Administrateur/stocks" render={() => (!this.isAdmin ?
              (<Redirect to="/"/>) : (<Stocks />))}/>
            <Route path="/Administrateur/emprunts" render={() => (!this.isAdmin ?
              (<Redirect to="/"/>) : (<Loanings />))}/>
            <Route exact path="/Administrateur/annonces" render={() => (!this.isAdmin ?
              (<Redirect to="/"/>) : (<Ads />))}/>
            <Switch>
              <Route path="/Administrateur/users/add" render={() => (!this.isAdmin ?
                (<Redirect to="/"/>) : (<AddUser />))}/>
              <Route path="/Administrateur/users/:idUser" render={({ history: {location}}) => (!this.isAdmin ?
                (<Redirect to="/"/>) : (<HandleUser location={location.pathname}/>))}/>
            </Switch>
            <Switch>
              <Route path="/Administrateur/stocks/add" render={() => (!this.isAdmin ?
                (<Redirect to="/"/>) : (<AddResource />))}/>
              <Route path="/Administrateur/stocks/:idStocks" render={({ history: {location}}) => (!this.isAdmin ?
                (<Redirect to="/"/>) : (<HandleResource location={location.pathname} />))}/>
            </Switch>
            <Switch>
              <Route path="/Administrateur/annonces/add" render={() => (!this.isAdmin ?
                (<Redirect to="/"/>) : (<AddAd />))}/>
              <Route path="/Administrateur/annonces/:idStocks"render={({ history: {location}}) => (!this.isAdmin ?
                (<Redirect to="/"/>) : (<HandleAd  location={location.pathname}/>))}/>
            </Switch>
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default HomePage;
