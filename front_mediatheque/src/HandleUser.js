import React, { Component } from "react";
import {Redirect} from "react-router-dom";
import axios from "axios";
import Loading from './Loading';



class RemoveUserButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect : false,
      isDeleting : false
    }
    this.handleRemove = this.handleRemove.bind(this);
  }

  handleRemove(event) {
    var self = this;
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    self.setState({ isDeleting: true });
    axios.delete(apiBaseUrl+'user/'+this.props.id, { withCredentials: true })
    .then(function (response) {
      if(response.status === 200) {
        self.setState({ redirect: true });
      }
    });
  }

  render() {
    const { redirect } = this.state;
    if (this.state.isDeleting) {
      if (redirect) {
            return <Redirect to={'/Administrateur/users'}/>;
      }
      return <Loading/>;
    }
    return (
        <button onClick={this.handleRemove}>
          supprimer cet utilisateur
        </button>
    );
  }
}

class HandleUser extends Component {
  render() {
    const path = this.props.location;
    const idUser = path.substr(path.lastIndexOf(('/')) + 1);
    return (
      <div>
        <RemoveUserButton id={idUser}/>

      </div>
    );
  }
}

export default HandleUser;
