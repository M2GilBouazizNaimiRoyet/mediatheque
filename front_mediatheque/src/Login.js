import React, { Component } from "react";
import {Redirect} from "react-router-dom";
import axios from "axios";

import Loading from './Loading';

class Login extends Component {
    constructor(props) {
      super(props);
      this.state = {
        mail : '',
        password : '',
        redirect: false,
        isChanging:false
      };

      this.handleChangeMail = this.handleChangeMail.bind(this);
      this.handleChangePassword = this.handleChangePassword.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeMail(event) {
      this.setState({mail: event.target.value});
    }

    handleChangePassword(event) {
      this.setState({password: event.target.value});
    }

    handleSubmit(event) {
      event.preventDefault();
      var apiBaseUrl = "http://localhost:8080/project-1.0/";
       var self = this;

       self.setState({ isChanging: true });
       axios.post(apiBaseUrl+'login?email='+this.state.mail
       +'&password='+this.state.password, {}, { withCredentials: true })
       .then(response => {
         if(response.status === 200) {
           window.sessionStorage.setItem("isLoggedIn","true");
           window.sessionStorage.setItem("userId",response.data.id);
           window.sessionStorage.setItem("mail",response.data.email);
           if (response.data.authority.name === "ADMIN") {
             window.sessionStorage.setItem("isAdmin","true");
           }
           self.setState({ redirect: true });
         }
       }, error => {
           if (error.response.status === 401) {
               alert("Mot de passe ou nom d'utilisateur incorrect");
               self.setState({isChanging: false});
           }
       });
    }

    render() {
      const { redirect } = this.state;
      if (this.state.isChanging) {
        if (redirect) {
          this.props.parent.setState({refresh:true});
          if(window.sessionStorage.getItem('isAdmin') === 'true') {
            return <Redirect to={'/Administrateur'}/>;
          } else {
            return <Redirect to={'/'}/>;
          }
        }
        return <Loading/>;
      }

      return (
        <div>
          <form onSubmit={this.handleSubmit}>
              <input type="email" placeholder="Mail"
                value={this.state.mail} onChange={this.handleChangeMail}/>
            <br/>
              <input type="password" placeholder="Mot de passe"
                value={this.state.password}onChange={this.handleChangePassword}/>
          <br/>
            <input type="submit" value="Se Connecter" />
          </form>
        </div>
      );
    }
  }


export default Login;
