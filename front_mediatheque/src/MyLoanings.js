import React, { Component } from "react";
import axios from "axios";
import Loading from './Loading';

class MyLoaningRow extends Component {
  render() {
    const loaning = this.props.loaning;
    return (
      <tr key={loaning.id}>
        <td >
          {loaning.resource.name}
        </td>
        <td>
          {loaning.resource.type}
        </td>
        <td>
          {loaning.date}
        </td>
        <td>
          {loaning.duration}
        </td>
      </tr>
    );
  }
}

class MyLoanings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loanings: [],
      isCharged : false
    };
  }

  componentDidMount(){
      var userId = window.sessionStorage.getItem('userId');
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    var self = this;
     axios.get(apiBaseUrl+'borrowing?userId='+userId, { withCredentials: true })
     .then(function (response) {
       if(response.status === 200){

         self.setState({
           loanings : response.data,
           isCharged : true
         })
       }
     });
  }

    componentDidUpdate(){
        var userId = window.sessionStorage.getItem('userId');
        var apiBaseUrl = "http://localhost:8080/project-1.0/";
        var self = this;
        axios.get(apiBaseUrl+'borrowing?userId='+userId, { withCredentials: true })
            .then(function (response) {
                if(response.status === 200 && JSON.stringify(self.state.loanings)!==JSON.stringify(response.data)){
                    self.setState({
                        loanings : response.data,
                        isCharged : true
                    })
                }
            });
    }

  render() {
    if (!this.state.isCharged) {
      return (<Loading/>);
    }
    const rows = [];
    this.state.loanings.forEach((loaning) => {
      rows.push(<MyLoaningRow loaning={loaning} key={loaning.id} />)
    });
    return (
        <table>
          <thead>
            <tr>
              <th>Nom</th>
              <th>Type</th>
              <th>Début</th>
              <th>Durée</th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
    );
  }
}

export default MyLoanings;
