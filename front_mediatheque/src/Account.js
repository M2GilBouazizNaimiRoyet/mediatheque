import React, { Component } from "react";
import {Redirect} from "react-router-dom";
import axios from "axios";
import Loading from './Loading';

class Account extends Component {
    constructor(props) {
      super(props);
      this.state = {
        oldPassword : '',
        password : '',
        redirect: false,
        isChanging: false
      };
      this.handleChangeOldPassword = this.handleChangeOldPassword.bind(this);
      this.handleChangePassword = this.handleChangePassword.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeOldPassword(event) {
      this.setState({oldPassword: event.target.value});
    }
    handleChangePassword(event) {
      this.setState({password: event.target.value});
    }

    handleSubmit(event) {
      event.preventDefault();
      var apiBaseUrl = "http://localhost:8080/project-1.0/";
      var self = this;
      var payload={
          "oldPassword":this.state.oldPassword,
          "password":this.state.password
       }
      this.setState({isChanging: true});
      axios.put(apiBaseUrl+'account', payload, { withCredentials: true })
      .then(function (response) {
        if(response.status === 200){
          alert("votre mot de passe a bien été modifié")
          self.setState({ redirect: true });
        }
      });

    }

    render() {
      if (this.state.isChanging) {
        const { redirect } = this.state;
        if (redirect) {
          return <Redirect to={'/'}/>;
        }
        return (<Loading/>);
      }
      return (
        <div>
          <form onSubmit={this.handleSubmit}>
              <input type="password" placeholder="Ancien mot de passe"
                value={this.state.oldPassword} onChange={this.handleChangeOldPassword}/>
            <br/>
              <input type="password" placeholder="Mot de passe"
                value={this.state.password}onChange={this.handleChangePassword}/>
          <br/>
            <input type="submit" value="Modifier" />
          </form>
        </div>
      );
    }
  }


export default Account;
