import React from 'react';
import ReactLoading from 'react-loading';

const Loading = () =>
    (<ReactLoading type="bars" color="#046380" className={"loading"}/>);

export default Loading;
