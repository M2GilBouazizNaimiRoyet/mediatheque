import React, { Component } from "react";
import {Redirect} from "react-router-dom";

class Logout extends Component {
  render() {
      sessionStorage.clear();
      this.props.parent.setState({refresh:false});
      return <Redirect to={'/'}/>;

  }
}

export default Logout;
