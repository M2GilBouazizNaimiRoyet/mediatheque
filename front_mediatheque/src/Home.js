import React, { Component } from "react";
import axios from "axios";
import Loading from './Loading';

class Ad extends Component {

  render() {
    const ad = this.props.ad;
    return (
      <section className="ad">
          <p dangerouslySetInnerHTML={{__html: ad.content}}/>
      </section>
    );
  }
}

class AdsDisplay extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ads: [],
      isCharged : false
    };
  }

  componentDidMount(){
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    var self = this;
     axios.get(apiBaseUrl+'ad', { withCredentials: true })
     .then(function (response) {
       if(response.status === 200){
         self.setState({
           ads : response.data,
           isCharged : true
         })
       }
     });
  }

  render() {
    if (!this.state.isCharged) {
      return (<Loading/>);
    } else {
      const rows = [];
      this.state.ads.forEach((ad) => {
        rows.push(<Ad ad={ad} key={ad.id} />);
      });

      return (
          <div>
            {rows}
          </div>
      );
    }
  }
}


class Home extends Component {
  render() {

      return (
        <div>
          <table>
            <tbody>
              <tr>
                <td>
                  <iframe src="https://feed.mikle.com/widget/v2/62850/" height="402px"
                    width="100%" className="fw-iframe" scrolling="no" frameBorder="0"></iframe>
                </td>
                <td>
                  <iframe src="https://feed.mikle.com/widget/v2/62852/" height="402px" width="100%"
                    className="fw-iframe" scrolling="no" frameBorder="0"></iframe>
                </td>
                <td>
                  <iframe src="https://feed.mikle.com/widget/v2/62853/" height="402px" width="100%"
                          className="fw-iframe" scrolling="no" frameBorder="0"></iframe>
                </td>
              </tr>
            </tbody>
          </table>

          <iframe seamless width="888" height="336" frameBorder="0"
            src="http://www.infoclimat.fr/public-api/mixed/iframeSLIDE?_ll=48.85341,2.34
            88&_inc=WyJQYXJpcyIsIjQyIiwiMjk4ODUwNyIsIkZSIl0=&_auth=AxkDFFIsV3UALVtsA3UEL
            QJqV2JaLAcgAn4EZwhtUy5WPQdmVDRSNAdpVyoOIQQyAC0AYwoxV2dTOFEpDX9QMQNpA29SOVcwA
            G9bPgMsBC8CLFc2WnoHIAJnBGEIe1MzVjAHfVQ2UjIHdlcwDjsELwAsAGEKMFduUzVRNA1mUDQDZ
            QNmUjVXKgBwWzwDYgQ1AjBXNVpgB20CMAQxCGJTNlZjB2dUMlIuB21XPA44BDIANABiCjxXZlMvU
            SkNGVBAA30DJ1JzV2AAKVskA2YEbgJl&_c=f32e243ed381957e1a5d5bdf47fae476"></iframe>
          <h2>Annonces</h2>
          <AdsDisplay/>
        </div>
      );
    }
}

export default Home;
