import React, { Component } from "react";
import { EditorState, convertToRaw} from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import {Redirect} from "react-router-dom";
import draftToHtml from 'draftjs-to-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import axios from "axios";
import Loading from './Loading';


class AddAd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState : EditorState.createEmpty(),
      redirect: false,
      isChanging:false

    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  onEditorStateChange: Function = (editorState) => {
    this.setState({
      editorState,
    });
  };

  handleSubmit(event) {
    event.preventDefault();
    const { editorState } = this.state;
    var self = this
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    var payload={
       "content":draftToHtml(convertToRaw(editorState.getCurrentContent()))
    }
    self.setState({ isChanging: true });
    axios.post(apiBaseUrl+'ad', payload, { withCredentials: true })
      .then(function (response) {
        if(response.status === 201) {
          self.setState({ redirect: true });
        }
      });
  }

  render() {
    const { editorState } = this.state;
    const { redirect } = this.state;
    if (this.state.isChanging) {
      if (redirect) {
            return <Redirect to={'/Administrateur/annonces'}/>;
      }
      return <Loading/>;
    }
    return (
      <form onSubmit={this.handleSubmit}>
        <Editor
          editorState={editorState}
          toolbarClassName="toolbarClassName"
          wrapperClassName="demo-wrapper"
          editorClassName="demo-editor"
          onEditorStateChange={this.onEditorStateChange}
          localization={{
            locale: 'fr',
          }}
        />
      <input type="submit" value="Ajouter" />
    </form>

  );
  }
}

export default AddAd;
