import React, { Component } from "react";
import {Link} from "react-router-dom";
import axios from "axios";
import styles from './index.css';
import Loading from './Loading';
import FileSaver from 'file-saver';

class UserRow extends Component {

  render() {
    const user = this.props.user;
    return (
      <tr key={user.id}>
        <td>
          <Link className={styles.contenttd} to={`/Administrateur/users/${user.id}`}>{user.email}</Link>
        </td>
        <td>
          <Link className={styles.contenttd} to={`/Administrateur/users/${user.id}`}>{user.authority.name}</Link>
        </td>
      </tr>
    );
  }
}

class UsersTab extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
      isCharged : false
    };
  }

  componentDidMount(){
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    var self = this;
     axios.get(apiBaseUrl+'user', { withCredentials: true })
     .then(function (response) {
       if(response.status === 200){
         self.setState({
           users : response.data,
           isCharged : true
         })
       }
     });
  }

    componentDidUpdate(){
        var apiBaseUrl = "http://localhost:8080/project-1.0/";
        var self = this;
        axios.get(apiBaseUrl+'user', { withCredentials: true })
            .then(function (response) {
                if(response.status === 200 && JSON.stringify(self.state.users)!==JSON.stringify(response.data)){
                    self.setState({
                        users : response.data,
                        isCharged : true
                    })
                }
            });
    }

  render() {
    if (!this.state.isCharged) {
      return (<Loading/>);
    }
    const rows = [];
    this.state.users.forEach((user) => {
      rows.push(<UserRow user={user} key={user.id} />);
    });
    return (
        <table>
          <thead>
            <tr>
              <th>Mail</th>
              <th>Rôle</th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
    );
  }
}

class AddUserButton extends Component {
  render() {
    return (
      <Link to={`/Administrateur/users/add`}>
        <button>
          ajouter un utilisateur
        </button>
      </Link>
    );
  }
}

class ExportUsersButton extends Component {
    constructor(props) {
        super(props);
        this.handleExport = this.handleExport.bind(this);
    }


    handleExport(event) {
        var apiBaseUrl = "http://localhost:8080/project-1.0/";
        axios.get(apiBaseUrl+'user/export/json', { withCredentials: true }).then((response) => {
            var blob = new Blob([JSON.stringify(response.data)], {type: "application/json;charset=utf-8"});
            FileSaver.saveAs(blob, 'users.json');
        });
    }

    render() {
        return (
            <button onClick={this.handleExport}>
                exporter les utilisateurs
            </button>
        );
    }
}

class Users extends Component {
  render() {
    return (
      <div>
        <AddUserButton/>
        <UsersTab/>
        <ExportUsersButton/>
      </div>
    )
  }
}

export default Users;
