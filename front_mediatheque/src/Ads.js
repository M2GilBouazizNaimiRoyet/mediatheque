import React, { Component } from "react";
import {Link} from "react-router-dom";
import axios from "axios";
import styles from './index.css';
import Loading from './Loading';


class Ad extends Component {
  render() {
    const ad = this.props.ad;
    return (
      <section class="ad">
          <p>
            <Link className={styles.contenttd}
              to={`/Administrateur/annonces/${ad.id}`}
              dangerouslySetInnerHTML={{__html: ad.content}}/>
          </p>
      </section>
    );
  }
}

class AdsDisplay extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ads: [],
      isCharged : false
    };
  }

  componentDidMount(){
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    var self = this;
     axios.get(apiBaseUrl+'ad', { withCredentials: true })
     .then(function (response) {
       if(response.status === 200){
         self.setState({
           ads : response.data,
           isCharged : true
         })
       }
     });
  }

    componentDidUpdate(){
        var apiBaseUrl = "http://localhost:8080/project-1.0/";
        var self = this;
        axios.get(apiBaseUrl+'ad', { withCredentials: true })
            .then(function (response) {
                if(response.status === 200 && JSON.stringify(self.state.ads)!==JSON.stringify(response.data)){
                    self.setState({
                        ads : response.data,
                        isCharged : true
                    })
                }
            });
    }

  render() {
    if (!this.state.isCharged) {
      return (<Loading/>);
    } else {
      const rows = [];
       this.state.ads.forEach((ad) => {
         rows.push(<Ad ad={ad} key={ad.id} />);
       });
      return (
          <div>
            {rows}
          </div>
      );
    }
  }
}

class AddAdButton extends Component {
  render() {
    return (
      <Link to={`/Administrateur/annonces/add`}>
        <button>
          ajouter une annonce
        </button>
      </Link>
    );
  }
}

class Ads extends Component {
  render() {
    return (
      <div>
        <AddAdButton/>
        <AdsDisplay/>
      </div>
    )
  }
}

export default Ads;
