import React, { Component } from "react";
import {Redirect} from "react-router-dom";
import axios from "axios";
import Loading from './Loading';

class ModifyResource extends Component {

  constructor(props) {
    super(props);
      this.state = {
        resource : undefined,
        redirect: false,
        isChanging:false,
        isCharged:false
      };

      this.handleChangeName = this.handleChangeName.bind(this);
      this.handleChangeType = this.handleChangeType.bind(this);
      this.handleChangeQuantity = this.handleChangeQuantity.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeName(event) {
    const resource = this.state.resource
    resource.resource.name = event.target.value;
    this.setState({resource});
  }

  handleChangeType(event) {
    const resource = this.state.resource
    resource.resource.type = event.target.value;
    this.setState({resource});
  }

  handleChangeQuantity(event) {
    const resource = this.state.resource
    resource.quantity = event.target.value;
    this.setState({resource});
  }

  handleSubmit(event) {
    event.preventDefault();
    var self = this;
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    var payload={
      "quantity":this.state.resource.quantity,
       "resource": {
         "type":this.state.resource.resource.type,
         "name":this.state.resource.resource.name
       }
    }
    self.setState({ isChanging: true });
    axios.put(apiBaseUrl+'stock/'+this.props.id, payload, { withCredentials: true })
    .then(function (response) {
      if(response.status === 200) {
        self.setState({ redirect: true });
      }
    });
  }

  componentDidMount() {
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    var self = this;
     axios.get(apiBaseUrl+'stock/'+this.props.id, { withCredentials: true })
     .then(function (response) {
       if(response.status === 200){
         self.setState({
           resource : response.data,
           isCharged:true
         });
       }
     });

  }

  render() {
      const { redirect } = this.state;
      if (!this.state.isCharged) {
        return (<Loading/>);
      }
      if (this.state.isChanging) {
        if (redirect) {
              return <Redirect to={'/Administrateur/stocks'}/>;
        }
        return <Loading/>;
      }
      return (
        <div>
          <form onSubmit={this.handleSubmit}>
            <label>
              Nom :
              <input type="text"
                value={this.state.resource.resource.name} onChange={this.handleChangeName}/>
            </label>
            <br/>
            <label>
              Type :
              <select value={this.state.resource.resource.type}
                onChange={this.handleChangeType}>
                <option value="DVD">DVD</option>
                <option value="CD">CD</option>
                <option value="VideoGame">jeu vidéo</option>
                <option value="BoardGame">jeu de société</option>
                <option value="Book">livre</option>
              </select>
            </label>
            <br/>
            <label>
              Quantité :
              <input type="number"
                value={this.state.resource.quantity} onChange={this.handleChangeQuantity}/>
            </label>
            <br/>
            <input type="submit" value="Modifier" />
          </form>
        </div>
      );
  }
}

class RemoveResourceButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect : false,
      isDeleting : false
    }
    this.handleRemove = this.handleRemove.bind(this);
  }

  handleRemove(event) {
    var self = this;
    var apiBaseUrl = "http://localhost:8080/project-1.0/";
    self.setState({ isDeleting: true });
    axios.delete(apiBaseUrl+'stock/'+this.props.id, { withCredentials: true })
    .then(function (response) {
      if(response.status === 200) {
        self.setState({ redirect: true });
      }
    });
  }

  render() {
    const { redirect } = this.state;
    if (this.state.isDeleting) {
      if (redirect) {
            return <Redirect to={'/Administrateur/stocks'}/>;
      }
      return <Loading/>;
    }
    return (
        <button onClick={this.handleRemove}>
          supprimer cette ressource
        </button>
    );
  }
}

class HandleResource extends Component {
  render() {
    const path = this.props.location;
    const idResource = path.substr(path.lastIndexOf(('/')) + 1);
    return (
      <div>
        <RemoveResourceButton id={idResource}/>
        <ModifyResource id={idResource}/>
      </div>
    );
  }
}

export default HandleResource;
