package fr.univ.jee.purchasecenter;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ.jee.purchasecenter.rest.PurchaseCenterRestControler;
import org.apache.log4j.Logger;
import spark.servlet.SparkApplication;

import static spark.Spark.*;

public class PurchaseCenterApplication implements SparkApplication {

    private static final ObjectMapper om = new ObjectMapper();

    private static final Logger logger = Logger.getLogger(PurchaseCenterApplication.class);

    public static void main(String[] args) {
        new PurchaseCenterApplication().init();

    }

    @Override
    public void init() {
        get("/", (request, response) -> {
            return "test";
        });
        PurchaseCenterRestControler purchaseCenterRestControler = new PurchaseCenterRestControler(om);
    }
}