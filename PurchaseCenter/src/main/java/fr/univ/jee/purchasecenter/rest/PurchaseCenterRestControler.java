package fr.univ.jee.purchasecenter.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ.jee.purchasecenter.domain.Stock;
import fr.univ.jee.purchasecenter.domain.resource.BoardGame;
import fr.univ.jee.purchasecenter.domain.resource.Book;
import fr.univ.jee.purchasecenter.domain.resource.Resource;
import org.eclipse.jetty.http.HttpStatus;

import fr.univ.jee.purchasecenter.domain.Response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static spark.Spark.*;

public class PurchaseCenterRestControler {

    private ObjectMapper om;

    private static List<Stock> order = new ArrayList<>();
    {
        Book b = new Book();
        b.setName("un livre");

        BoardGame bg = new BoardGame();
        bg.setName("un jeu de société");

        Stock bS = new Stock();
        bS.setQuantity(5);
        bS.setResource(b);

        Stock bgS = new Stock();
        bgS.setQuantity(5);
        bgS.setResource(bg);


        order.add(bS);
        order.add(bgS);
    }

    public PurchaseCenterRestControler(final ObjectMapper om) {
        this.om = om;


        after((req, res) -> {
            res.type("application/json");
        });

        handleExceptions();


        initializeController();
    }

    private void initializeController() {
        get("/order", (request, response) -> {
            response.status(200);
            return om.writeValueAsString(order);
        });

        post("/order/stock", (request, response) -> {
            Stock r = om.readValue(request.body(), Stock.class);
            order.add(r);
            response.status(200);
            Response respMessage = new Response("stock ajoutée à la commande");
            return om.writeValueAsString(respMessage);
        });

        delete("/order", (request, response) -> {
            order.clear();
            response.status(200);
            return om.writeValueAsString(new Response("commande vidée"));
        });
    }



    protected void handleExceptions() {

        exception(Exception.class, (ex, req, res) -> {
            res.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            try {
                res.body(om.writeValueAsString(new Response(ex.getMessage())));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
    }

}
