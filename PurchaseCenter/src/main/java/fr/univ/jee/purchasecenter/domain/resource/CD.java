package fr.univ.jee.purchasecenter.domain.resource;


public class CD extends Resource {

    private String albumName;

    public CD() {
        super();
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

}
